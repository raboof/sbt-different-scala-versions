lazy val root = (project in file("."))
  .aggregate(util, core)

lazy val util = (project in file("util"))
  .settings(
    crossScalaVersions := Seq("2.12.14", "2.13.7"),
  )

lazy val core = (project in file("core"))
  .settings(
    crossScalaVersions := Seq("2.12.14"),
  )
  .dependsOn(util)
