This example project simulates a scenario where 'core' depends on
'util', and 'util' supports more Scala versions than 'core'.

This works fine with `sbt "++2.13.7 clean" "++2.13.7 compile"`.

However, this means you have to keep the Scala patch version consistent between
your `crossScalaVersions` and your CI build scripts, and can be inconvenient
when switching between branches.

Previously we used a system property and some logic in the
`scalaVersion` setting to select the right Scala patch version, but
that doesn't work in the scenario described in this example project,
see the `with-system-property` branch of this repo for a demonstration.

It looks like we can pull this off with a custom command, however,
see the `with-command` branch of this repo!
